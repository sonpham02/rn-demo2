/**
 * @providesModule ScrollableTabsView
 */

import React, {Component} from 'react';
import {
    Text,
    View,
    Platform
} from 'react-native';

import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
var ScrollableTabBarCustom = require('./android/ScrollableTabBarCustom');
var ScrollViewTest = require('ScrollViewTestEx');

var ScrollableTabs = React.createClass({

    renderTabBar() {
        if(Platform.OS === 'ios') {
            return (<ScrollableTabBar/>);
        } else {
            return (<ScrollableTabBarCustom/>);
        }
    },

    render() {
        return (
            <ScrollableTabView
                initialPage={0}
                tabBarBackgroundColor="#193546"
                tabBarActiveTextColor="#8ca5ac"
                tabBarInactiveTextColor="#748896"
                tabBarUnderlineColor="#e3652b"
                renderTabBar={this.renderTabBar}>
                <ScrollViewTest tabLabel='Nổi bật' uri='https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E'>
                    My
                </ScrollViewTest>
                <ScrollViewTest tabLabel='Super Brands'
                                uri='https://scontent-dfw1-1.xx.fbcdn.net/t31.0-8/12998315_10206321744586692_7334286642015631717_o.jpg'
                >favorite</ScrollViewTest>
                <ScrollViewTest tabLabel='Chương trình khuyến mại'
                                uri='https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/12494867_10206137048649409_5338068746520228522_n.jpg?oh=2a5e753009145f03cf0dc1beb16b8c7b&oe=582FFBD9'>
                    hello
                </ScrollViewTest>
                <Text tabLabel='Điện tử'>hi</Text>
                <Text tabLabel='Nhà Cửa & Đời sống'>alo</Text>
                <Text tabLabel='Làm đẹp'>balo</Text>
                <Text tabLabel='Thời Trang'>Dwdwt</Text>
                <Text tabLabel='Thể thao du lịch'>pr32132oject</Text>
                <Text tabLabel='Mẹ & Bé'>pro312312ject</Text>
            </ScrollableTabView>
        )
    }
});

module.exports = ScrollableTabs;
