var PixelRatio = require('PixelRatio');
var SCREEN_HEIGHT = require('Dimensions').get('window').height;
var buildStyleInterpolator = require('buildStyleInterpolator');


var FromTheRight = {
    opacity: {
        value: 1.0,
        type: 'constant',
    },

    transformTranslate: {
        from: {x: require('Dimensions').get('window').width, y: 0, z: 0},
        to: {x: 0, y: 0, z: 0},
        min: 0,
        max: 1,
        type: 'linear',
        extrapolate: true,
        round: PixelRatio.get(),
    },

    translateX: {
        from: require('Dimensions').get('window').width,
        to: 0,
        min: 0,
        max: 1,
        type: 'linear',
        extrapolate: true,
        round: PixelRatio.get(),
    },

    scaleX: {
        value: 1,
        type: 'constant',
    },
    scaleY: {
        value: 1,
        type: 'constant',
    },
};
var FromTheTop = {
    ...FromTheRight,
    transformTranslate: {
        from: {y: -SCREEN_HEIGHT, x: 0, z: 0},
        to: {x: 0, y: 0, z: 0},
        min: 0,
        max: 1,
        type: 'linear',
        extrapolate: true,
        round: PixelRatio.get(),
    },
    translateY: {
        from: -SCREEN_HEIGHT,
        to: 0,
        min: 0,
        max: 1,
        type: 'linear',
        extrapolate: true,
        round: PixelRatio.get(),
    },
};

var NoTransition = {
    opacity: {
        from: 1,
        to: 1,
        min: 1,
        max: 1,
        type: 'linear',
        extrapolate: false,
        round: 100
    }
};



var CustomSceneConfig =  {
    // A very tighly wound spring will make this transition fast
    springTension: 200,
    springFriction: 26,

    defaultTransitionVelocity: 15,
    // Use our custom gesture defined above
    //gestures: {
    //    pop: CustomGestures
    //},
    animationInterpolators: {
        into: buildStyleInterpolator(FromTheTop),
        out: buildStyleInterpolator(NoTransition)
    }
};

module.exports = {
    NoTransition,
    CustomSceneConfig
};
