/**
 * @providesModule ScrollViewTestEx
 */

import React, {Component} from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    StyleSheet,
    Platform,
    Image
} from 'react-native';

var THUMBS = ['https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E',
    'https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E',
    'https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E',
    'https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E',
    'https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E',
    'https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E',
    'https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E',
    'https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E',
    'https://scontent-dfw1-1.xx.fbcdn.net/v/t1.0-9/13055470_10206364659019526_6241052552831368855_n.jpg?oh=6558bf1ffc6b1a91d06e049d405c4a22&oe=582F226E'];
THUMBS = THUMBS.concat(THUMBS); // double length of THUMBS

class Thumb extends React.Component{
    shouldComponentUpdate(nextProps, nextState) {
        return false;
    }
    render() {
        return (
            <View style={styles.button}>
                <Image style={styles.img} source={{uri:this.props.uri}} />
            </View>
        );
    }
}
var createThumbRow = function (uri, i) {
    return (<Thumb key={i} uri={uri} />);
};



var cloneChildren = function (child) {
    var ret = [];
    for (var i = 0; i < 10; i++) {
        ret.push(child);
    }
    return ret;
};

var ScrollViewTest =  React.createClass({
    render() {
        return (
            <ScrollView>
                {cloneChildren(this.props.uri).map(createThumbRow)}
            </ScrollView>
        )
    }
});

var styles = StyleSheet.create({
    scrollView: {
        backgroundColor: '#6A85B1',
        height: 300,
    },
    horizontalScrollView: {
        height: 120,
    },
    containerPage: {
        height: 50,
        width: 50,
        backgroundColor: '#527FE4',
        padding: 5,
    },
    text: {
        fontSize: 20,
        color: '#888888',
        left: 80,
        top: 20,
        height: 40,
    },
    button: {
        margin: 7,
        padding: 5,
        alignItems: 'center',
        backgroundColor: '#eaeaea',
        borderRadius: 3,
    },
    buttonContents: {
        flexDirection: 'row',
        width: 64,
        height: 64,
    },
    img: {
        width: 64,
        height: 64,
    }
});

module.exports = ScrollViewTest;
