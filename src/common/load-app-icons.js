
import Ionicons from 'react-native-vector-icons/Ionicons';

const icons = {
    "ios-person": 30,
    "md-search": 30,
    "ios-cart-outline": 30
};

let iconsMap = {};
let iconsLoaded = new Promise((resolve, reject) => {
    new Promise.all(
        Object.keys(icons).map(iconName => Ionicons.getImageSource(iconName, icons[iconName]))
    ).then(sources => {
        console.log(sources);
        Object.keys(icons).forEach((iconName, idx) => iconsMap[iconName] = sources[idx]);
        resolve(true);
    })
});

export {
    iconsMap,
    iconsLoaded
}