/**
 * @providesModule NavBarIOS
 */

import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Ionicons';

class navBarIOS extends React.Component {

    constructor(props) {
        super(props);
    }

    _onPressButton() {
        this.props.navigator.toggleDrawer({
            side: 'left',
            animated: true
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <View style={{ marginLeft:15}}>
                        <TouchableOpacity onPress={this._onPressButton.bind(this)}>
                            <FontAwesome name="bars" size={20} color="white"/>
                        </TouchableOpacity>

                    </View>
                    <View style={{marginLeft: 10}}>
                        <Image
                            source={require('../../../img/added-react.png')}
                            resizeMode={Image.resizeMode.contain}/>
                    </View>
                    <View style={styles.searchRow}>
                        <View style={{backgroundColor:'white', justifyContent:'center', borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingLeft: 5}}>
                            <Icon name="ios-search" size={19} color="#d3d3d3" />
                        </View>
                        <View style={{flex:1, backgroundColor:'white', borderTopRightRadius: 5, borderBottomRightRadius: 5}}>
                            <TextInput
                                autoCapitalize="none"
                                autoCorrect={false}
                                clearButtonMode="while-editing"
                                placeholder="Tìm kiếm sản phẩm..."
                                style={styles.textInput}
                            />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
};

var styles = StyleSheet.create({
    container: {
        height: 64,
        backgroundColor:'#193546'
    },
    row: {
        flex:1,
        marginTop: 20,
        flexDirection: 'row',
        alignItems:'center'
    },
    searchRow: {
        marginLeft: 10, flex:1, flexDirection: 'row', marginRight: 10
    },
    textInput: {height: 30, fontSize: 15, paddingLeft: 5}
});

module.exports = navBarIOS;