import React, {Component} from 'react';
import {
    AppRegistry,
    View
} from 'react-native';
import { Navigation } from 'react-native-navigation';

// screen related book keeping
import { registerScreens } from './screens';
import {iconsMap, iconsLoaded} from './common/load-app-icons';
registerScreens();


iconsLoaded.then(() => {

    Navigation.startTabBasedApp({
        tabs: [
            {
                label: 'One',
                screen: 'example.FirstTabScreen',
                icon: require('../img/one.png'),
                selectedIcon: require('../img/one_selected.png'),
                title: 'Screen One',
                navigatorButtons : {
                    leftButtons: [{
                        icon: require('../img/one.png'),
                        id: 'menu'
                    }],

                    rightButtons: [
                        {
                            icon: iconsMap['ios-cart-outline'],
                            id: 'io'
                        }
                    ]
                }
            },
            {
                // label: 'Three',
                screen: 'example.ThirdTabScreen',
                icon: iconsMap['md-search'],
                selectedIcon: iconsMap['md-search'],
                // title: 'Screen Three',

                navigatorButtons : {
                    rightButtons: [
                        {
                            icon: iconsMap['ios-cart-outline'],
                            id: 'io'
                        }
                    ]
                }
            }
        ],
        // tabsStyle: {
        //   tabBarButtonColor: '#ffff00',
        //   tabBarSelectedButtonColor: '#ff9900',
        //   tabBarBackgroundColor: '#551A8B'
        // },
        drawer: {
            left: {
                screen: 'example.SideMenu'
            }
        },
        // animationType: 'slide-down'
    });
});

