import React, {Component} from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    StyleSheet,
    AlertIOS,
    StatusBar,
    Platform
} from 'react-native';

var NavBarIOS = require('NavBarIOS');
var ScrollableTabsView = require('ScrollableTabsView');

export default class FirstTabScreen extends Component {
    static navigatorStyle = {
        drawUnderTabBar: true,
        navBarHidden: true
    };
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                     barStyle="light-content"/>
                <NavBarIOS navigator={this.props.navigator}/>
                <ScrollableTabsView/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        textAlign: 'center',
        fontSize: 18,
        marginBottom: 10,
        marginTop:10,
        color: 'blue'
    },
    container: {
        flex: 1
    }
});

