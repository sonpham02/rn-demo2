import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

export default class ThirdTabScreen extends Component {
  static navigatorStyle = {

    navBarTextColor: '#000000', // change the text color of the title (remembered across pushes)
    navBarBackgroundColor: '#f7f7f7', // change the background color of the nav bar (remembered across pushes)
    navBarButtonColor: '#007aff', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
    navBarHidden: false, // make the nav bar hidden
    navBarHideOnScroll: false, // make the nav bar hidden only after the user starts to scroll
    navBarTranslucent: true, // make the nav bar semi-translucent, works best with drawUnderNavBar:true
    navBarTransparent: true, // make the nav bar transparent, works best with drawUnderNavBar:true
    navBarNoBorder: false, // hide the navigation bar bottom border (hair line). Default false
    drawUnderNavBar: true, // draw the screen content under the nav bar, works best with navBarTranslucent:true
    drawUnderTabBar: false, // draw the screen content under the tab bar (the tab bar is always translucent)
    statusBarBlur: false, // blur the area under the status bar, works best with navBarHidden:true
    navBarBlur: false, // blur the entire nav bar, works best with drawUnderNavBar:true
    tabBarHidden: false, // make the screen content hide the tab bar (remembered across pushes)
    statusBarHideWithNavBar: false ,// hide the status bar if the nav bar is also hidden, useful for navBarHidden:true
    statusBarHidden: false, // make the status bar hidden regardless of nav bar state
    statusBarTextColorScheme: 'dark'
  };
  constructor(props) {
    super(props);
    this.state = {
      navBarVisability: 'shown'
    }
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{height: 64, backgroundColor: '#193546'}}>
          <Text style={styles.text}>12</Text>
        </View>
        <TouchableOpacity onPress={ this.onPushSlideScene.bind(this) }>
          <Text style={styles.button}>Push Slide Screen</Text>
        </TouchableOpacity>
      </View>
    );
  }
  onPushPress() {
    this.props.navigator.push({
      title: "More",
      screen: "example.PushedScreen",
    });
  }

  onPushSlideScene() {
    this.props.navigator.push({
      title: "1111111",
      screen: "example.SlideScreen",
      passProps: {title: "SlideScreen"}
    })
  }

  onPushStyledPress() {
    this.props.navigator.push({
      title: "Styled",
      screen: "example.StyledScreen"
    });
  }
  onPushStyled2Press () {
    this.props.navigator.push({
      title: "Styled",
      titleImage: require('../../img/two.png'),
      screen: "example.StyledScreen"
    });
  }
  onModalPress() {
    this.props.navigator.showModal({
      title: "Modal",
      screen: "example.ModalScreen"
    });
  }

  onToggleNavBarPressed() {
    this.state.navBarVisability = (this.state.navBarVisability === 'shown') ? 'hidden' : 'shown';
    this.props.navigator.toggleNavBar({
      to: this.state.navBarVisability,
      animated: true  // true is default
    });
  }

  componentDidUpdate() {
    console.error('this is an error: ' + Math.random());
    this.state.navBarState = 'shown';
  }

}

const styles = StyleSheet.create({
  button: {
    textAlign: 'center',
    fontSize: 18,
    marginBottom: 10,
    marginTop:10,
    color: 'blue'
  },
  text: {
    flex: 1,
    position: 'absolute',
    top: 32,
    right: 21,
    fontSize: 8,
    color: 'white'
  }
});
