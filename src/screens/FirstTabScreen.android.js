import React, {Component} from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    StyleSheet,
    Platform,
    TextInput
} from 'react-native';

var ScrollableTabsView = require('ScrollableTabsView');

import Icon from 'react-native-vector-icons/Ionicons';
import {iconsMap} from '../common/load-app-icons';

console.log(iconsMap);

export default class FirstTabScreen extends Component {

    static navigatorStyle = {
        drawUnderTabBar: true,
        navBarBackgroundColor: '#193546',
        navBarNoBorder: true,
        navBarButtonColor: '#ffffff',
        navBarTextColor: '#ffffff',
        // navBarHidden: true
    };
    constructor(props) {
        super(props);
        // if you want to listen on navigator events, set this up
    }

    render() {
        return (
            <View style={{flex:1}}>
                <View style={{backgroundColor: '#193546'}}>
                    <View style={{ flexDirection: 'row', marginRight: 20, marginLeft: 20 }}>
                        <View style={{backgroundColor:'white', justifyContent:'center', borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingLeft: 5}}>
                            <Icon name="md-search" size={23}  />
                        </View>
                        <View style={{flex:1, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5}}>
                            <TextInput
                                autoCapitalize="none"
                                autoCorrect={false}
                                clearButtonMode="while-editing"
                                placeholder="Tìm kiếm sản phẩm..."
                                style={{height: 50, fontSize: 14, paddingLeft: 7}}
                            />
                        </View>
                    </View>
                </View>
                <ScrollableTabsView/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        textAlign: 'center',
        fontSize: 18,
        marginBottom: 10,
        marginTop:10,
        color: 'blue'
    }
});