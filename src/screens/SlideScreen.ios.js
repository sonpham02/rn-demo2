import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    TouchableNativeFeedback,
    Image,
    Platform,
    SegmentedControlIOS,
    ScrollView,
    Navigator,
} from 'react-native';

var NavigatorNavigationBarStyles = Platform.OS === 'android' ?
    null : require('../common/NavigatorNavigationBarStylesIOSCustom');
var SCREEN_HEIGHT = require('Dimensions').get('window').height;

var TransitionConfig = require('../common/TransitionCustomConfig');
var BaseConfig = Navigator.SceneConfigs.VerticalDownSwipeJump;
var CustomSceneConfig = Object.assign({}, BaseConfig, TransitionConfig.CustomSceneConfig);
import Icon from 'react-native-vector-icons/Ionicons';

var SwipeScene = require('./SwipeScene');

var MockUpText = React.createClass({
    render: function () {
        return(
            <View style={{marginTop: 10}}>
                <View style={{marginTop: 10, marginBottom: 10}}>
                    <Text>Lorem Ipsum</Text>
                    <Text>Lorem Ipsum</Text>
                    <Text>99999999</Text>
                </View>

                <ScrollView style={styles.scrollView}>
                    <Text>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

                        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                    </Text>
                </ScrollView>
                <View style={{backgroundColor: 'rgba(0, 0, 0, 0.1)', height: 1.5}}/>
                <View>
                    <Text>Lorem Ipsum</Text>
                </View>
            </View>
        )
    }
});

var MainScene = React.createClass({

    getInitialState() {
        return {
            values: ['Detail', 'Testimonial'],
            selectedIndex: 0
        }
    },

    _handlePress() {
        this.props.navigator.pop();
    },

    render() {
        return (
            <View style={[styles.container,{backgroundColor: 'white'}]}>
                <View style={{margin: 10}}>
                    <SegmentedControlIOS
                        values={this.state.values}
                        selectedIndex={this.state.selectedIndex}
                    />
                    <MockUpText/>
                </View>
            </View>
        )
    }
});

class SlideScene extends Component {
    static navigatorStyle = {
        drawUnderTabBar: true,
        navBarHidden: true
    };

    constructor(props) {
        super(props);
    }

    _renderScene(route, navigator) {
        if(route.id && route.id == 2) {
            return <MainScene navigator={navigator}/>
        }
        if(route.id && route.id == 1) {
            return <SwipeScene navigator={navigator}/>
        }
    }

    _configureScene() {
        return CustomSceneConfig;
    }

    render() {
        return (
            <Navigator
                initialRoute={{
                    id: 1,
                    title: 'Hello'
                }}
                renderScene={this._renderScene}
                configureScene={this._configureScene}
                navigationBar={
                    <Navigator.NavigationBar style={{backgroundColor: '#4dbce9'}}
                                         routeMapper={NavigationBarRouteMapper(this.props)}
                                         navigationStyles={NavigatorNavigationBarStyles}/>
                 }
            />
        )
    }
}

NavigationBarRouteMapper = (props) => ({
    LeftButton(route, navigator, index, navState) {

        if(index == 0) {
            if(props.title) {
                return (
                    <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}
                                      onPress={() => props.navigator.pop()}>
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Icon name="ios-arrow-back" size={35} color={'#ffffff'} style={{marginLeft: 5, marginTop: 2}}/>
                            <Text style={{color: 'white', marginLeft: 5, fontSize: 17}}>
                                {props.title}
                            </Text>
                        </View>
                    </TouchableOpacity>
                );
            }
        }

        const previousRoute = navState.routeStack[index - 1];
        return (
            <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}
                              onPress={() => navigator.pop()}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <Icon name="ios-arrow-back" size={35} color={'#ffffff'} style={{marginLeft: 5, marginTop: 2}}/>
                    <Text style={{color: 'white', marginLeft: 5, fontSize: 17}}>
                        {previousRoute.title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    },
    RightButton(route, navigator, index, navState) {
        //if(route.rightElement) {
        //    return route.rightElement;
        //}
        return null;
    },
    Title(route, navigator, index, navState) {
        return (
            <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{color: 'white', margin: 10, fontSize: 18}}>
                    {route.title}
                </Text>
            </View>
        );
    }
});



const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:   NavigatorNavigationBarStyles.General.TotalNavHeight
    },
    scrollView: {
        height: 0.3 * SCREEN_HEIGHT,
        marginBottom: 15
    }
});

module.exports = SlideScene;