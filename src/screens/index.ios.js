import { Navigation } from 'react-native-navigation';

import FirstTabScreen from './FirstTabScreen';
import ThirdTabScreen from './ThirdTabScreen';
import SideMenu from './SideMenu';
import SlideScreen from './SlideScreen.ios';

// register all screens of the app (including internal ones)
export function registerScreens() {
  Navigation.registerComponent('example.FirstTabScreen', () => FirstTabScreen);
  Navigation.registerComponent('example.ThirdTabScreen', () => ThirdTabScreen);
  Navigation.registerComponent('example.SideMenu', () => SideMenu);
  Navigation.registerComponent('example.SlideScreen', () => SlideScreen);
}
