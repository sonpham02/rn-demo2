package com.swipesegmented;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import java.util.Arrays;
import java.util.List;

import com.reactnativenavigation.activities.RootActivity;
import com.reactnativenavigation.packages.RnnPackage;
import com.oblador.vectoricons.VectorIconsPackage;

public class MainActivity extends RootActivity {

    /**
     * A list of packages used by the app. If the app uses additional views
     * or modules besides the default ones, add more packages here.
     */
    @Override
    public List<ReactPackage> getPackages() {
        return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RnnPackage(),
            new VectorIconsPackage()
        );
    }
}
